package controlador;

import modelo.Logica;
import vista.VentanaPresentacion;
import vista.Ventana_Ingreso_Manual;
import vista.Ventana_Resultados;
import vista.Ventana_Visualizar_Grafo;

public class Principal 
{
	private VentanaPresentacion ventanaPresentacion;
	private Ventana_Ingreso_Manual ventanaIngresoManual;
	private Ventana_Resultados ventanaResultados;
	private Ventana_Visualizar_Grafo ventanaVisualizarGrafo;
	private Controlador ctrl;
	private Logica logica;

	public static void main(String[] args) 
	{
		Principal ppl = new Principal();
		ppl.iniciar();
	}
	
	private void iniciar() {
		ventanaPresentacion = new VentanaPresentacion();
		ventanaIngresoManual = new Ventana_Ingreso_Manual();
		ventanaResultados = new Ventana_Resultados();
		ventanaVisualizarGrafo = new Ventana_Visualizar_Grafo();
		ctrl = new Controlador();
		logica = new Logica();
		
		ventanaPresentacion.setControlador(ctrl);
		ventanaIngresoManual.setControlador(ctrl);
		ventanaResultados.setControlador(ctrl);
		ventanaVisualizarGrafo.setControlador(ctrl);
		
		ctrl.setLogica(logica);
		ctrl.setVentana_ingreso_manual(ventanaIngresoManual);
		ctrl.setVentana_principal(ventanaPresentacion);
		ctrl.setVentana_resultados(ventanaResultados);
		ctrl.setVentana_visualizar_grafo(ventanaVisualizarGrafo);
		
		ventanaPresentacion.setVisible(true);
	}
	
}
