package controlador;

import modelo.Lienzo;
import modelo.Logica;
import vista.VentanaPresentacion;
import vista.Ventana_Ingreso_Manual;
import vista.Ventana_Resultados;
import vista.Ventana_Visualizar_Grafo;

public class Controlador 
{
	private VentanaPresentacion ventana_principal;
	private Ventana_Ingreso_Manual ventana_ingreso_manual;
	private Ventana_Resultados ventana_resultados;
	
	private Ventana_Visualizar_Grafo ventana_visualizar_grafo;
	private Logica logica;
	private Lienzo lienzo;
	
	public void setVentana_visualizar_grafo(Ventana_Visualizar_Grafo ventana_visualizar_grafo) {
		this.ventana_visualizar_grafo = ventana_visualizar_grafo;
	}
		
	public void setVentana_principal(VentanaPresentacion ventana_principal) {
		this.ventana_principal = ventana_principal;
	}
	
	public void setVentana_ingreso_manual(Ventana_Ingreso_Manual ventana_ingreso_manual) {
		this.ventana_ingreso_manual = ventana_ingreso_manual;
	}
	
	public void setVentana_resultados(Ventana_Resultados ventana_resultados) {
		this.ventana_resultados = ventana_resultados;
	}
	
	public void setLogica(Logica logica) 
	{
		this.logica = logica;
	}
	
	public Logica getLogica() {
		return this.logica;
	}
		
/////////////METODOS PROPIOS///////////////
	
	public void mostrarVentanaPrincipal() 
	{
		this.ventana_principal.setVisible(true);
	}
	
	public void mostrarVentanaCargaManual() {
		this.ventana_ingreso_manual.setVisible(true);
	}
	
	public void mostrarVentana_Resultados()
	{
		this.ventana_resultados.setVisible(true);
	}
	
	public void mostrarVentanaVisualizarGrafo() {
		this.ventana_visualizar_grafo.mostrar(true);
	}
	
	public void crearGrafo(Object vertices) 
	{
		this.logica.crearGrafo(vertices);
	}
		
	public boolean solverVacio() 
	{
		return this.logica.solverVacio();
	}
	
	public void iniciarSolver() 
	{
		this.logica.solver();
	}
	
	public void limpiar() 
	{
		this.logica.limpiarTodo();
	}
	
	public void ingresarArista(int a, int b) {
		this.logica.ingresarAristas(a, b);
	}
	
	public boolean existeArista(int a, int b) 
	{
		return this.logica.existeArista(a, b);
	}
	
	public void asociarPeso(String peso, Object vertice) 
	{
		this.logica.asociar_peso_vertice(vertice, peso);
	}
	
	public void calcular() 
	{
		long inicialTime = System.nanoTime();
		this.logica.calcular_clique_de_peso_maximo(this.logica.getSolver().verticePesado());
		this.logica.pesar_vertices_de_clique();
		long finalTime = System.nanoTime() - inicialTime;
		
		this.ventana_resultados.actualizar(this.logica.getPeso(), this.logica.getClique(),finalTime);
	}
	
	public void cargarLienzo() {
		this.lienzo = new Lienzo(this.logica);
		this.lienzo.set_log(this.logica);
		this.ventana_visualizar_grafo.setLienzo(this.lienzo);
		this.ventana_visualizar_grafo.repaint();
	}
		
}
