package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.Controlador;

import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Ventana_Resultados extends JFrame implements ActionListener{

	private Controlador ctrl;
	private JPanel contentPane;
	private JLabel label_vertices, lblPesoTotal, lblTiempo;
	private JTextField txt_pesoTotal, txt_vertices, txt_tiempo;
	private JButton volver_a_calcular, btn_visualizar;

	public Ventana_Resultados() {
		setTitle("Programacion III Trabajo Practico 3 Cliques Golosas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 510, 390);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		label_vertices = new JLabel("Vertices: ");
		label_vertices.setBounds(42, 138, 190, 25);
		contentPane.add(label_vertices);
		
		lblPesoTotal = new JLabel("Peso Total: ");
		lblPesoTotal.setBounds(42, 80, 190, 25);
		contentPane.add(lblPesoTotal);
		
		lblTiempo = new JLabel("Tiempo: ");
		lblTiempo.setBounds(42, 196, 190, 25);
		contentPane.add(lblTiempo);
		
		volver_a_calcular = new JButton("Calcular nuevamente");
		volver_a_calcular.setBounds(300, 307, 184, 33);
		contentPane.add(volver_a_calcular);
		
		btn_visualizar = new JButton("Visualizar Grafo");
		btn_visualizar.setBounds(300, 139, 184, 41);
		contentPane.add(btn_visualizar);
		
		txt_pesoTotal = new JTextField();
		txt_pesoTotal.setHorizontalAlignment(SwingConstants.CENTER);
		txt_pesoTotal.setText("0.0");
		txt_pesoTotal.setEditable(false);
		txt_pesoTotal.setBounds(114, 82, 104, 20);
		contentPane.add(txt_pesoTotal);
		txt_pesoTotal.setColumns(10);
		
		txt_vertices = new JTextField();
		txt_vertices.setHorizontalAlignment(SwingConstants.CENTER);
		txt_vertices.setText("[]");
		txt_vertices.setEditable(false);
		txt_vertices.setBounds(114, 140, 104, 20);
		contentPane.add(txt_vertices);
		txt_vertices.setColumns(10);
		
		txt_tiempo = new JTextField();
		txt_tiempo.setHorizontalAlignment(SwingConstants.CENTER);
		txt_tiempo.setText("00:00");
		txt_tiempo.setEditable(false);
		txt_tiempo.setBounds(114, 198, 104, 20);
		contentPane.add(txt_tiempo);
		txt_tiempo.setColumns(10);
		
		volver_a_calcular.addActionListener(this);
		btn_visualizar.addActionListener(this);
		
	}
	
	public void setControlador(Controlador ctrl) 
	{
		this.ctrl = ctrl;
	}
	
	public void actualizar(String peso, String vertices, long tiempo) 
	{
		this.txt_vertices.setText(vertices);
		this.txt_pesoTotal.setText(peso);
		this.txt_tiempo.setText(String.valueOf(tiempo/1e6)+"ms");
	}
	

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == this.volver_a_calcular) 
		{
			ctrl.mostrarVentanaPrincipal();
			this.dispose();
		}
		
		if(e.getSource() == this.btn_visualizar) 
		{
			ctrl.cargarLienzo();
			ctrl.mostrarVentanaVisualizarGrafo();
		}
	}
}
