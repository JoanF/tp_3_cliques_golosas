package vista;


import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import controlador.Controlador;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Font;

public class VentanaPresentacion extends JFrame implements ActionListener{
	
	private Controlador ctrl;
	
	private JButton btn_siguiente;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel;

	public VentanaPresentacion() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Trabajo Practico III Cliques Golosas");
		setBounds(100, 100, 510, 390);
		
		
		lblNewLabel = new JLabel("Grupo N\u00BA9 \"LLamalo como quieras\"");
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 20));
		lblNewLabel.setBounds(71, 91, 328, 25);
		getContentPane().add(lblNewLabel);
		
		btn_siguiente = new JButton("Siguiente");
		btn_siguiente.setBounds(369, 291, 109, 44);
		getContentPane().add(btn_siguiente);
		
		btn_siguiente.addActionListener(this);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
		
		lblNewLabel_1 = new JLabel("Integrantes: ");
		lblNewLabel_1.setBounds(71, 189, 82, 33);
		getContentPane().add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("Joan Fernando Ene");
		lblNewLabel_2.setBounds(165, 189, 152, 16);
		getContentPane().add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Tahiel Giordana");
		lblNewLabel_3.setBounds(165, 217, 109, 16);
		getContentPane().add(lblNewLabel_3);

	}

	public void setControlador(Controlador miControlador) {
		this.ctrl = miControlador;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{	
		
		if(e.getSource() == this.btn_siguiente) 
		{
			ctrl.mostrarVentanaCargaManual();
			this.dispose();
		}
	}
}
