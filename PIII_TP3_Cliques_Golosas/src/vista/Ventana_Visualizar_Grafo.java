package vista;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controlador.Controlador;
import modelo.Arista;
import modelo.Grafo;
import modelo.Lienzo;
import modelo.Logica;
import modelo.Vertice;

public class Ventana_Visualizar_Grafo extends JPanel {
	private Controlador ctrl;
	private Lienzo lienzo;
	private JFrame jf;
	
	
	public void paintComponent(Graphics g) 
	{
		Random r = new Random();
		super.paintComponent(g);
		//System.out.println(lienzo.getVertices().size());
		for(Vertice v: lienzo.getVertices()){
			v.setPosition(r.nextInt(500)+15, r.nextInt(500)+15);
			g.setColor(v.get_color());
			g.fillOval(v.getX()-15, v.getY()-15, 30, 30);
			g.setColor(Color.BLACK);
			g.drawString(v.get_numVertice()+" \nPeso: "+ v.get_peso(), v.getX()-15, v.getY()-15);			
		}
		
		for(Arista a : lienzo.getAristas()) {
				g.setColor(Color.BLACK);
				g.drawLine(a.get_v1().getX(), a.get_v1().getY(), a.get_v2().getX(), a.get_v2().getY());			
		}
		
		for(Arista b : lienzo.getClique()) {
			g.setColor(Color.RED);
			g.drawLine(b.get_v1().getX(), b.get_v1().getY(), b.get_v2().getX(), b.get_v2().getY());
		}
			

	}
	
	public Ventana_Visualizar_Grafo()
	{
		this.ctrl = new Controlador();
		this.lienzo = new Lienzo(ctrl.getLogica());
		jf = new JFrame();
		jf.setTitle("Grafo-Clique M�xima");
		jf.setBounds(100, 100, 600, 600);
		jf.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		jf.setVisible(false);
		jf.add(this);	
	}
	
	public void setControlador(Controlador c) {
		this.ctrl = c;
	}
	
	public void setLienzo(Lienzo l) {
		this.lienzo = l;
	}
	
	public void mostrar(Boolean b) {
		this.jf.setVisible(b);
	}
	
}
