package vista;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import controlador.Controlador;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;

public class Ventana_Ingreso_Manual extends JFrame implements ActionListener
{
	private Controlador ctrl = new Controlador();

	private JLabel lbl_ingreso_de_pesos, lbl_aristas_ingresadas, lbl_ingreso_aristas, vertice_con_peso;
	private JButton btn_calcular, btn_limpiar, btn_establecer, btn_ingresar, btn_confirmar;
	
	private JTextField txt_double_peso;
	private JSpinner arista_A, arista_B, cantidad_vertices, vertices_confirmados;
	private JComboBox aristas;
	
/////////////CONSTRUCTOR/////////////////
	public Ventana_Ingreso_Manual() {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Trabajo Practico III Cliques Golosas");
		setBounds(100, 100, 510, 390);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
	
		btn_calcular = new JButton("Calcular");
		btn_calcular.setBounds(388, 304, 89, 23);
		getContentPane().add(btn_calcular);
		
		btn_limpiar = new JButton("Limpiar");
		btn_limpiar.setBounds(24, 301, 95, 28);
		getContentPane().add(btn_limpiar);
		
		btn_ingresar = new JButton("Ingresar");
		btn_ingresar.setBounds(291, 197, 95, 28);
		btn_ingresar.setEnabled(false);
		getContentPane().add(btn_ingresar);
		
		btn_establecer = new JButton("Establecer");
		btn_establecer.setBounds(376, 70, 95, 28);
		btn_establecer.setEnabled(false);
		getContentPane().add(btn_establecer);
		
		txt_double_peso = new JTextField();
		txt_double_peso.setBounds(112, 197, 50, 28);
		txt_double_peso.setColumns(10);
		txt_double_peso.setEnabled(false);
		getContentPane().add(txt_double_peso);
		
		lbl_ingreso_aristas = new JLabel("Ingrese las aristas: ");
		lbl_ingreso_aristas.setBounds(24, 76, 170, 16);
		getContentPane().add(lbl_ingreso_aristas);
		
		arista_A = new JSpinner();
		arista_A.setModel(new SpinnerNumberModel(0, 0, 9, 1));
		arista_A.setEnabled(false);
		arista_A.setBounds(206, 70, 73, 28);
		getContentPane().add(arista_A);
		
		arista_B = new JSpinner();
		arista_B.setModel(new SpinnerNumberModel(0, 0, 9, 1));
		arista_B.setEnabled(false);
		arista_B.setBounds(291, 70, 73, 28);
		getContentPane().add(arista_B);
		
		lbl_aristas_ingresadas = new JLabel("Aristas ingresadas");
		lbl_aristas_ingresadas.setBounds(24, 129, 122, 16);
		getContentPane().add(lbl_aristas_ingresadas);
		
		aristas = new JComboBox();
		aristas.setEditable(false);
		aristas.setBounds(151, 122, 95, 30);
		getContentPane().add(aristas);
		
		JLabel lblNewLabel = new JLabel("Seleccione la cantidad de vertices de su grafo: ");
		lblNewLabel.setBounds(24, 21, 290, 16);
		getContentPane().add(lblNewLabel);
		
		btn_confirmar = new JButton("Confirmar");
		btn_confirmar.setBounds(375, 15, 102, 28);
		getContentPane().add(btn_confirmar);
		
		cantidad_vertices = new JSpinner();
		cantidad_vertices.setModel(new SpinnerNumberModel(1, 1, 10, 1));
		cantidad_vertices.setBounds(314, 15, 50, 28);
		getContentPane().add(cantidad_vertices);
		
		vertices_confirmados = new JSpinner();
		vertices_confirmados.setModel(new SpinnerNumberModel(1, 1, 10, 1));
		vertices_confirmados.setEnabled(false);
		vertices_confirmados.setBounds(233, 197, 50, 28);
		getContentPane().add(vertices_confirmados);
		
		lbl_ingreso_de_pesos = new JLabel("Ingresar peso ");
		lbl_ingreso_de_pesos.setBounds(24, 204, 102, 14);
		getContentPane().add(lbl_ingreso_de_pesos);
		
		JLabel lbl_ingreso_de_pesos_1 = new JLabel("al vertice: ");
		lbl_ingreso_de_pesos_1.setBounds(174, 204, 67, 14);
		getContentPane().add(lbl_ingreso_de_pesos_1);
		
		vertice_con_peso = new JLabel("");
		vertice_con_peso.setHorizontalAlignment(SwingConstants.CENTER);
		vertice_con_peso.setBounds(24, 237, 181, 16);
		getContentPane().add(vertice_con_peso);
		
		btn_limpiar.addActionListener(this);
		btn_confirmar.addActionListener(this);
		btn_establecer.addActionListener(this);
		btn_ingresar.addActionListener(this);
		btn_calcular.addActionListener(this);

	}
	
	public void setControlador(Controlador miControlador) {
		this.ctrl = miControlador;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		////////BOTON CONFIRMAR CANTIDAD DE VERTICES/////////////
		if(e.getSource().equals(this.btn_confirmar)) 
		{
			ctrl.crearGrafo(this.cantidad_vertices.getValue());
			habilitar();
			actualizarDatos();
			
		}///////FIN CONFIRMAR CANTIDAD DE VERTICES///////////////
		
		//////////////BOTON ESTABLECER ARISTA////////////////////
		if(e.getSource().equals(btn_establecer) ) 
		{
			
			int a = (Integer)this.arista_A.getValue();
			int b = (Integer)this.arista_B.getValue();
			
			if( a == b ) 
			{
				JOptionPane.showMessageDialog(null,"No se permiten loops","Error",JOptionPane.ERROR_MESSAGE);
			}
			else 
			{
				if(ctrl.existeArista(a,b) == false)
				{
					ctrl.ingresarArista(a,b);
					String arista = String.valueOf(a)+ "__" + String.valueOf(b);
					this.aristas.addItem(arista);
				}
			}
		}////////////////FIN ESTABLECER ARISTA///////////////////

		////////////////BOTON LIMPIAR////////////////////////////
		if(e.getSource() == this.btn_limpiar) 
		{
			deshabilitar();
			ctrl.limpiar();
		}///////////////FIN BOTON LIMPIAR///////////////////
		
		////////////////BOTON PARA INGRESAR PESOS //////////////////////
		if(e.getSource().equals(this.btn_ingresar)) 
		{
			this.cantidad_vertices.setEnabled(false);
			this.btn_confirmar.setEnabled(false);
			this.arista_A.setEnabled(false);
			this.arista_B.setEnabled(false);
			this.btn_establecer.setEnabled(false);
			if(this.txt_double_peso.getText().equals("")) 
			{
				JOptionPane.showMessageDialog(null,"Debe ingresar un valor","Error",JOptionPane.ERROR_MESSAGE);

			}
			else {
					if(ctrl.solverVacio()) 
					{	
						ctrl.iniciarSolver();
						ingresarPeso();
				
					}   else 
						{
							ingresarPeso();
						}
			}
		}/////////////FIN BOTON PARA INGRESAR PESOS////////////////
		
		/////////////BOTON CALCULAR//////////////////
		if(e.getSource() == this.btn_calcular)
		{
			if(ctrl.solverVacio()) 
			{
				JOptionPane.showMessageDialog(null,"Debe ingresar datos para calcular","Error",JOptionPane.ERROR_MESSAGE);
			}
			else 
			{
				ctrl.calcular();
				ctrl.mostrarVentana_Resultados();
				this.dispose();
				
			}
			
		}///////////FIN BOTON CALCULAR//////////////
	}

	private void ingresarPeso() {
		ctrl.asociarPeso(this.txt_double_peso.getText(), this.vertices_confirmados.getValue());
		this.vertice_con_peso.setText(" Vertice: "+ this.vertices_confirmados.getValue() + "   Peso = "+ this.txt_double_peso.getText());
		this.txt_double_peso.setText("");
	}



	private void deshabilitar() {
		this.cantidad_vertices.setEnabled(true);
		this.btn_confirmar.setEnabled(true);
		
		this.arista_A.setEnabled(false);
		this.arista_B.setEnabled(false);
		this.aristas.removeAllItems();
		this.btn_establecer.setEnabled(false);
		this.txt_double_peso.setText("");
		this.txt_double_peso.setEnabled(false);
		this.vertices_confirmados.setEnabled(false);
		this.btn_ingresar.setEnabled(false);
	}

	private void habilitar() 
	{
		this.cantidad_vertices.setEnabled(false);
		this.btn_confirmar.setEnabled(false);
		
		this.arista_A.setEnabled(true);
		this.arista_B.setEnabled(true);
		this.btn_establecer.setEnabled(true);
		this.txt_double_peso.setEnabled(true);
		this.vertices_confirmados.setEnabled(true);
		this.btn_ingresar.setEnabled(true);
	}
	
	private void actualizarDatos() 
	{
		int cantidad_de_vertices_confirmados = ((Integer)this.cantidad_vertices.getValue()) -1;
		
		arista_A.setModel(new SpinnerNumberModel(0, 0,cantidad_de_vertices_confirmados, 1));
		arista_B.setModel(new SpinnerNumberModel(0, 0,cantidad_de_vertices_confirmados, 1));
		this.vertices_confirmados.setModel(new SpinnerNumberModel(0, 0,cantidad_de_vertices_confirmados, 1));
	}
}
