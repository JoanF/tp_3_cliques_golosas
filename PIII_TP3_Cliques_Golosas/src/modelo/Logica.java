package modelo;

import java.util.Set;

public class Logica {
	
	private Grafo grafo;
	private Solver sol;
	private Set<Integer> clique_encontrada;
	private double peso_maximo = 0.0;
	
	
	////////////////GETTERS///////////
	public Solver getSolver() 
	{
		return this.sol;
	}
	
	public Grafo getGrafo() 
	{
		return this.grafo;
	}
	
	public String getClique()
	{
		if(this.clique_encontrada == null) 
		{
			return null;
		}
		
		return this.clique_encontrada.toString();
	}
	
	public Set<Integer> getCliqueList()
	{
		return this.clique_encontrada;
	}
	
	public String getPeso()
	{
		return String.valueOf(this.peso_maximo);
	}
	
	////////////////////////////////////////////
	
	public void crearGrafo(Object vertices) 
	{
		int v = (Integer) vertices;
		this.grafo = new Grafo(v);
	}
	
	public void solver() 
	{
		this.sol = new Solver(this.grafo);
	}
	
	public void ingresarAristas(int a, int b) 
	{
		this.grafo.agregarArista(a, b);
	}
	
	public boolean existeArista(int a, int b) 
	{
		return this.grafo.existeArista(a, b);
	}
	
	public void asociar_peso_vertice(Object vertice, String d) 
	{
		int v = (int)vertice;
		this.sol.asociar_Vertice_Peso(v, Double.parseDouble(d));	
	}
	
	public void limpiarTodo() 
	{
		this.grafo = null;
		this.sol = null;
		this.clique_encontrada = null;
		this.peso_maximo = 0.0;
	}
	
	public boolean solverVacio() 
	{
		return this.sol == null;
	}
	
	public void calcular_clique_de_peso_maximo(int vertice)
	{
		this.clique_encontrada = this.sol.funcion_voraz(vertice);
	} 
	
	
	public void pesar_vertices_de_clique() 
	{
		for(Integer i : this.clique_encontrada) 
		{
			this.peso_maximo = peso_maximo + this.sol.getPeso(i);
		}
	}

	
	
}
