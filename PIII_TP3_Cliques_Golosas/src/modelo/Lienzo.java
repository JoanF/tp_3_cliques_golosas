package modelo;

import java.util.ArrayList;
import java.util.TreeSet;

public class Lienzo {
	Logica _log;
	ArrayList<Vertice> vertices;
	ArrayList<Arista> aristas;
	ArrayList<Arista> clique;
	
	public Lienzo(Logica l){
		this._log = l;
		vertices = new ArrayList<Vertice>();
		aristas = new ArrayList<Arista>();
		clique = new ArrayList<Arista>();
	}
		
	public Logica get_log() {
		return _log;
	}

	public void set_log(Logica _log) {
		this._log = _log;
		cargarLienzo();
	}

	public ArrayList<Vertice> getVertices() {
		return vertices;
	}

	public void setVertices(ArrayList<Vertice> vertices) {
		this.vertices = vertices;
	}

	public ArrayList<Arista> getAristas() {
		return aristas;
	}

	public void setAristas(ArrayList<Arista> aristas) {
		this.aristas = aristas;
	}
	

	public ArrayList<Arista> getClique() {
		return clique;
	}

	public void setClique(ArrayList<Arista> clique) {
		this.clique = clique;
	}

	public void cargarLienzo() {
		Grafo g = _log.getGrafo();
		Solver sol = _log.getSolver();
		for(int i=0; i<g.tamanio(); i++) {
			double peso = sol.getPeso(i);			
			vertices.add(new Vertice(i,peso));
		}
		for(int i=0; i<g.tamanio();i++) {
			TreeSet<Integer> vecinos = g.vecinos(i);
			for(Integer j : vecinos) {
				aristas.add(new Arista(vertices.get(i),vertices.get(j)));
			}
		}
		for(Integer i : _log.getCliqueList()) {
			for (Integer j : _log.getCliqueList() ) {
				if(i != j) {
					clique.add(new Arista(vertices.get(i),vertices.get(j)));
				}
			}
		}
	}
		
}
