package modelo;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

public class SolverTest 
{

	@Test(expected = Exception.class)
	public void vertice_inexistenteTest() 
	{
		Solver sol = new Solver(new Grafo(3));
		sol.asociar_Vertice_Peso(3, 2.0);
	}
	
	@Test(expected = Exception.class)
	public void peso_negativoTest() 
	{
		Solver sol = new Solver(new Grafo(3));
		sol.asociar_Vertice_Peso(0, -5.2);
	}
	
	@Test
	public void vertice_mas_pesadoTest() 
	{
		Solver sol = new Solver(new Grafo(5));
		sol.asociar_Vertice_Peso(0, 2.0);
		sol.asociar_Vertice_Peso(2, 1.0);
		sol.asociar_Vertice_Peso(1, 5.0);
		sol.asociar_Vertice_Peso(4, 1.0);
		sol.asociar_Vertice_Peso(3, 5.0);
		
		assertEquals(1,sol.verticePesado());
	}
	
	@Test
	public void pesos_iguales_todos_aisladosTest() 
	{
		Solver sol = new Solver(new Grafo(4));
		sol.asociar_Vertice_Peso(0, 1.0);
		sol.asociar_Vertice_Peso(1, 1.0);
		sol.asociar_Vertice_Peso(2, 1.0);
		sol.asociar_Vertice_Peso(3, 1.0);

		int[] esperado = {0};
		Set<Integer> obtenido = sol.funcion_voraz(0);
		
		iguales(esperado, obtenido);
	}
	
	@Test
	public void pesos_nulos_todos_aisladosTest() 
	{
		Solver sol = new Solver(new Grafo(4));
		
		assertEquals(0, sol.verticePesado());
	}
	
	@Test
	public void grafo_completo_con_pesos_nulosTest()
	{
		Grafo g = k4();
		
		Solver sol = new Solver(g);
		
		int[] esperado = {0,1,2,3};
		
		iguales(esperado, sol.funcion_voraz(0));
	}

	
	@Test
	public void grafo_completo_con_pesos_distintosTest()
	{
		Grafo g = k4();
		
		Solver sol = new Solver(g);
		sol.asociar_Vertice_Peso(0, 2.0);
		sol.asociar_Vertice_Peso(1, 4.0);
		sol.asociar_Vertice_Peso(2, 3.0);
		sol.asociar_Vertice_Peso(3, 1.0);
		
		int[] esperado = {0,1,2,3};
		
		iguales(esperado, sol.funcion_voraz(0));
	}
	
	@Test (expected = Exception.class)
	public void indice_invalidoTest() 
	{
		Solver sol = new Solver(k4());
		sol.asociar_Vertice_Peso(0, 2.0);
		sol.asociar_Vertice_Peso(1, 1.0);
		sol.asociar_Vertice_Peso(2, 7.0);
		sol.asociar_Vertice_Peso(3, 3.0);
		
		double peso = sol.getPeso(4);
	}
	
	private Grafo k4() 
	{
		Grafo g = new Grafo(4);
		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(0, 3);
		g.agregarArista(2, 1);
		g.agregarArista(2, 3);
		g.agregarArista(3, 1);
		return g;
	}
	
	private static void iguales(int[] esperado, Set<Integer> obtenido)
	{
		assertEquals(esperado.length, obtenido.size());
		
		for(int i=0; i<esperado.length; ++i)
			assertTrue( obtenido.contains(esperado[i]) );
	}

}
