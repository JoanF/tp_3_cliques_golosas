package modelo;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.Before;
import org.junit.Test;

public class LogicaTest 
{
	private Logica l1;
	
	@Before
	public void setUp() throws Exception
	{
		l1 = new Logica();
		
		//usamos el grafo del enunciado para las pruebas
		
		////(1)Se crea el grafo
		l1.crearGrafo(6);
		
		////Se ingresan las aristas
		l1.ingresarAristas(0, 1);
		l1.ingresarAristas(0, 3);
		l1.ingresarAristas(1, 2);
		l1.ingresarAristas(1, 3);
		l1.ingresarAristas(1, 4);
		l1.ingresarAristas(1, 5);
		l1.ingresarAristas(2, 4);
		l1.ingresarAristas(3, 4);
		l1.ingresarAristas(3, 5);
		l1.ingresarAristas(4, 5);
		
		/////(2)Pasamos como parametro el grafo al solver para iniciarlo
		l1.solver();
		
		/////Asociamos los pesos con sus respectivos vertices
		l1.asociar_peso_vertice(0, "11.0");
		l1.asociar_peso_vertice(1, "5.5");
		l1.asociar_peso_vertice(2, "1.1");
		l1.asociar_peso_vertice(3, "7.0");
		l1.asociar_peso_vertice(4, "2.5");
		l1.asociar_peso_vertice(5, "3.5");
		
		/////(3)Calculamos la clique con los pesos ingresados. Pasamos como parametro el vertice mas pesado...
		//////... y calculamos las cliques a partir de ese vertice.
		l1.calcular_clique_de_peso_maximo(0);
		
		////(4)pesamos los vertices de la clique
		l1.pesar_vertices_de_clique();
	}
	
	@Test
	public void getPesoMaximoTest() 
	{
		String esperado = "23.5";
		String obtenido = l1.getPeso();
		
		assertEquals(esperado,obtenido);
	}
	
	@Test
	public void limpiarTodoTest() 
	{
		l1.limpiarTodo();
		
		assertTrue(l1.solverVacio());
		assertNull(l1.getSolver());
		assertNull(l1.getClique());
		assertEquals("0.0", l1.getPeso());
	}
	
	@Test
	public void clique_encontradaTest() 
	{
		String clique = "[0, 1, 3]";
		String obtenido = l1.getClique();
		
		assertEquals(clique,obtenido);
	}
	
}
