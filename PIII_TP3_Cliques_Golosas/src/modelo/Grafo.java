package modelo;

import java.util.ArrayList;
import java.util.TreeSet;

public class Grafo 
{
	private ArrayList<TreeSet<Integer>> lista_de_vecinos;
	
	public Grafo(int vertices) 
	{
		this.lista_de_vecinos = new ArrayList<TreeSet<Integer>>();
		for(int i = 0; i<vertices; i++) {
			this.lista_de_vecinos.add(new TreeSet<Integer>());
		}
	}
	
	
	public TreeSet<Integer> vecinos(int vertice) 
	{
		return this.lista_de_vecinos.get(vertice);
	}
	
	public void agregarArista(int i, int j) 
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		this.lista_de_vecinos.get(i).add(j);
		this.lista_de_vecinos.get(j).add(i);
	}
	
	public void eliminarArista(int i, int j) 
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		this.lista_de_vecinos.get(i).remove(j);
		this.lista_de_vecinos.get(j).remove(i);
	}
	
	public boolean existeArista(int i, int j) 
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		return this.lista_de_vecinos.get(i).contains(j);
	}
	
	public int tamanio() 
	{
		return this.lista_de_vecinos.size();
	}
	
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= this.lista_de_vecinos.size() )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	void verificarDistintos(int i, int j)
	{
		if( i == j )
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}
}