package modelo;

import java.awt.Color;
import java.util.Random;

public class Vertice {
	int _numVertice;
	Color _color;
	double _peso;
	int posx,posy;
	
	public Vertice(int n, double d) {
		Random r = new Random();
		this._numVertice = n;
		this._peso = d;
		this._color = new Color(r.nextInt(200),r.nextInt(200),r.nextInt(200));
		posx = 0;
		posy = 0;
	}
	
	public void setPosition(int x, int y) {
		this.posx = x;
		this.posy = y;
	}
	
	public int getX() {
		return posx;
	}
	
	public int getY() {
		return posy;
	}
	

	public int get_numVertice() {
		return _numVertice;
	}

	public void set_numVertice(int _numVertice) {
		this._numVertice = _numVertice;
	}

	public Color get_color() {
		return _color;
	}

	public void set_color(Color _color) {
		this._color = _color;
	}

	public double get_peso() {
		return _peso;
	}

	public void set_peso(double _peso) {
		this._peso = _peso;
	}
}
