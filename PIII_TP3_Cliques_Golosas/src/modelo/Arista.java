package modelo;

import java.awt.Color;
import java.util.Random;

public class Arista {
	Vertice _v1,_v2;
	Color _c;
	
	public Arista(Vertice v1, Vertice v2) {
		this._v1 = v1;
		this._v2 = v2;
		Random r = new Random();
		this._c = new Color(r.nextInt(200),r.nextInt(200),r.nextInt(200));
	}

	public Vertice get_v1() {
		return _v1;
	}

	public void set_v1(Vertice _v1) {
		this._v1 = _v1;
	}

	public Vertice get_v2() {
		return _v2;
	}

	public void set_v2(Vertice _v2) {
		this._v2 = _v2;
	}
	
	public Color get_c() {
		return _c;
	}
	
}
