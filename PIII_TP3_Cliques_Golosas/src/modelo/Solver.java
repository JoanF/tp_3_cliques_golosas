package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Solver 
{
	private Grafo g;
	private ArrayList<Double> pesos;
	
	public Solver(Grafo grafo)
	{
		this.g = grafo;
		this.pesos = new ArrayList<Double>();
		for(int i = 0; i<grafo.tamanio();i++) 
		{
			this.pesos.add(0.0);
		}
	}
	
	public void asociar_Vertice_Peso(int vertice, double peso) 
	{
		verificarPeso(peso);
		this.pesos.set(vertice, peso);
	}

	public double getPeso(Integer i) 
	{
		verificar(i);
		return this.pesos.get(i);
	}

	public Set<Integer> funcion_voraz(int vertice) 
	{
		ArrayList<Double> auxiliar = OrdenDescendente();
		ArrayList<Double> copia = copiar();
		Set<Integer> vertices = new HashSet<Integer>();
		vertices.add(vertice);
		for(Double d : auxiliar)
		{
			int v = copia.indexOf(d);
			vertices.add(v);
			copia.set(v, null);
			
			if(!esClique(this.g, vertices))
			{
				vertices.remove(v);
			}
		}
		
		return vertices;
	}
	
	public int verticePesado() 
	{
		double aux = 0;
		int vertice = 0;
		for(int i=0; i<pesos.size();i++){
			if(pesos.get(i)>aux) {
				aux = pesos.get(i);
				vertice = i;
			}
		}
		return vertice;
	}
	
	private ArrayList<Double> copiar() 
	{
		ArrayList<Double> p = new ArrayList<Double>();
		for(Double d : this.pesos) 
		{
			p.add(d);
		}
		return p;
	}

	private ArrayList<Double> OrdenDescendente() 
	{
		ArrayList<Double> p = new ArrayList<Double>();
		for(Double d : this.pesos) 
		{
			p.add(d);
		}
		Collections.sort(p);
		Collections.reverse(p);
		return p;
	}
	
	private boolean esClique(Grafo grafo, Set<Integer> conjunto)
	{
		for(Integer i: conjunto)
		for(Integer j: conjunto)
		{
			if( i != j && grafo.existeArista(i, j) == false )
				return false;
		}
		
		return true;
	}
	
	private void verificar(Integer i) 
	{
		if(i<0 || i>this.pesos.size()) 
		{
			throw new RuntimeException("Indice inexistente");
		}
	}
	
	private void verificarPeso(double peso) 
	{
		if(peso<0) 
		{
			throw new RuntimeException("El peso no puede ser negativo");
		}
	}
	
}
